# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""
import numpy as np
import scipy.linalg as nla 
alpha,beta,gamma,mu=[0.5,-1,6,3]
Tx=np.array([6,5.5,-1,8,2,4.5])

def f(x):
     return alpha*x**3-beta*x**2+gamma*x+mu

Ty_courbe=np.array([f(x) for x in Tx])


ecart=np.array([1.5,-4.7,5,6.5,10,-1])
Ty_exp=Ty_courbe+ecart


T_res = Ty_courbe - Ty_exp
erreur_init=nla.norm(T_res,2) 

A=np.array([[x**3,x**2,x,1] for x in Tx])
b=Ty_courbe

Q1,R1= nla.qr(A, mode='economic')
Q1Tb=np.dot(np.transpose(Q1),Ty_exp)

alpha,beta,gamma,mu=nla.solve(R1,Q1Tb)
Ty_nouv_courbe=np.array([f(x) for x in Tx])

erreur_opt=nla.norm(Ty_nouv_courbe - Ty_exp,2)
print("erreur initial :",erreur_init)
print("erreur optimale :",erreur_opt)


