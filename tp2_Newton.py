#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 09:46:54 2022

@author: gmonsaur
"""
#on cherche racine cubique de 2
#x tel que x³=2
def f(x):
    return x**3-2

def fprime(x):
    return  3*x**2

u0=2.
for i in range(6):
    print('u[%d]='%i,u0)
    u1=u0-f(u0)/fprime(u0)
    u0=u1
